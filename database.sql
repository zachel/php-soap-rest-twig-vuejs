CREATE DATABASE paytest;
USE paytest;

-- Clientes
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`documento` VARCHAR(15) NOT NULL UNIQUE,
	`nombres` VARCHAR(75) NOT NULL,
	`email` VARCHAR(75) NOT NULL,
	`celular` VARCHAR(15) NOT NULL,
	`created_at` DATETIME DEFAULT NULL,
	`updated_at` DATETIME DEFAULT NULL
) ENGINE=InnoDB, CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Wallet
DROP TABLE IF EXISTS `wallet`;
CREATE TABLE `wallet` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`cliente` INT NOT NULL UNIQUE,
	`token` VARCHAR(40) NOT NULL UNIQUE,
	`balance` decimal(20,2) NOT NULL,
	`created_at` DATETIME DEFAULT NULL,
	`updated_at` DATETIME DEFAULT NULL,
	FOREIGN KEY(`cliente`) REFERENCES `clientes`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB, CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Transacciones
DROP TABLE IF EXISTS `transacciones`;
CREATE TABLE `transacciones` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `cliente` INT NOT NULL,
    `estatus` TINYINT(1) NOT NULL DEFAULT 0,
    `tipo` ENUM('pago','recarga') NOT NULL,
    `monto` DECIMAL(20,2) NOT NULL DEFAULT 0.00,
    `codigo` VARCHAR(6) NOT NULL,
	`created_at` DATETIME DEFAULT NULL,
	`updated_at` DATETIME DEFAULT NULL,
	FOREIGN KEY(`cliente`) REFERENCES `clientes`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB, CHARACTER SET utf8 COLLATE utf8_general_ci;


