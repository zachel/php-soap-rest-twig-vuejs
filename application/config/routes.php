<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['api'] = 'rest/index';
$route['api/server'] = 'ws/index';
$route['api/(:any)'] = 'wsclient/index/$1';
$route['v1'] = 'rest/index';
$route['v1/(:any)'] = 'rest/index/$1';
$route['(:any)'] = 'welcome/$1';