<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader {
    public function view($template, $data = array(), $return = FALSE) {
        $CI =& get_instance();

        try {
            $output = $CI->twig->render($template, $data);
        } catch (Exception $e) {
            show_error(htmlspecialchars_decode($e->getMessage()), 500, 'Twig Exception');
        }

        if ($return === TRUE) {
            return $output;
        }
        $CI->output->append_output($output);
    }
}