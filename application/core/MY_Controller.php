<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'libraries/JWT.php');
define('JWT_SECRET_KEY','bff09845ad6d655ad9293012cb70a62c312e6532');

class MY_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		try {
		}
		catch( Exception $e ){
		}
	}

	public function _phoneRegEx($number){
		if (preg_match('/^[0-9\(\)\-\+\.\s]{6,25}$/',$number)) {
			return TRUE;
		}
		else {
			$this->form_validation->set_message('_phoneRegEx','El número telefónico contiene caracteres inválidos.');
			return FALSE;
		}
	}

	public function unique($value, $params) {
		$fields = explode(',', $params);
		list($table, $field) = explode('.', $fields[0], 2);
		$this->db->select($field)
							 ->from($table)
							 ->where($field, $value)
							 ->limit(1);
		if (isset($fields[1])) {
			list($where_table, $where_field) = explode('.', $fields[1], 2);
			$where_value = $this->input->post($where_field);
			if (isset($where_value)) {
				$this->db->where("{$where_table}.{$where_field} <>", $where_value);
			}
		}
		$query = $this->db->get();
		if ($query->row()) {
			$this->form_validation->set_message('unique', 'Este valor ya esta registrado en el sistema.');
			return FALSE;
		}
		return TRUE;
	}
}
