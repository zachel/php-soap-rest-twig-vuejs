<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ws extends CI_Controller {

	var $server;
	var $ns;

	function __construct()
	{
		parent::__construct();
		$this->load->library('Nusoap');
		$this->server = new soap_server();
	    $this->server->soap_defencoding = 'UTF-8';
	    $this->server->decode_utf8 = false;
	    $this->server->encode_utf8 = true;
	    $this->ns="http://$_SERVER[SERVER_NAME]/api/server";
	    $this->server->configureWSDL('SOAP_API_Server', 'urn:server');
	}

	public function index()
	{ 
		// if (!isset($_GET['wsdl'])) { header('Location: /api/server?wsdl'); die(); }
		// SOAP Actions
		function registrarcliente($data) {
			$CI =& get_instance();
			return $CI->registrarCliente($data);
		}
		function recargarbilletera($data) {
			$CI =& get_instance();
			return $CI->recargarBilletera($data);
		}
		function consultarsaldo($data) {
			$CI =& get_instance();
			return $CI->consultarSaldo($data);
		}
		function realizarpago($data) {
			$CI =& get_instance();
			return $CI->realizarPago($data);
		}
		function confirmarpago($data) {
			$CI =& get_instance();
			return $CI->confirmarPago($data);
		}
		// SOAP Actions :END

	    $this->server->wsdl->schemaTargetNamespace=$this->ns;
	    
	    $this->server->wsdl->addComplexType('Cliente','complexType','struct','all','',
	    	array(
	    		'nombres'=>array('name'=>'nombres','type'=>'xsd:string'),
	    		'documento'=>array('name'=>'documento','type'=>'xsd:string'),
	    		'email'=>array('name'=>'email','type'=>'xsd:string'),
	    		'celular'=>array('name'=>'celular','type'=>'xsd:string')
	    	)
	    );

	    $this->server->wsdl->addComplexType('Saldo','complexType','struct','all','',
	    	array(
	    		'documento'=>array('name'=>'documento','type'=>'xsd:string'),
	    		'celular'=>array('name'=>'celular','type'=>'xsd:string')
	    	)
	    );

	    $this->server->wsdl->addComplexType('Recarga','complexType','struct','all','',
	    	array(
	    		'documento'=>array('name'=>'documento','type'=>'xsd:string'),
	    		'celular'=>array('name'=>'celular','type'=>'xsd:string'),
	    		'valor'=>array('name'=>'valor','type'=>'xsd:string')
	    	)
	    );

	    $this->server->wsdl->addComplexType('Pago','complexType','struct','all','',
	    	array(
	    		'documento'=>array('name'=>'documento','type'=>'xsd:string'),
	    		'compra'=>array('name'=>'compra','type'=>'xsd:string'),
	    		'monto'=>array('name'=>'monto','type'=>'xsd:string')
	    	)
	    );

		$this->server->wsdl->addComplexType('Confirmacion','complexType','struct','all','',
	    	array(
	    		'sesion'=>array('name'=>'sesion','type'=>'xsd:string'),
	    		'token'=>array('name'=>'token','type'=>'xsd:string')
	    	)
	    );

		$inputs = [
			'cliente'=>array('Cliente' => 'tns:Cliente'),
			'recarga'=>array('Recarga' => 'tns:Recarga'),
			'saldo'=>array('Saldo' => 'tns:Saldo'),
			'pago'=>array('Pago' => 'tns:Pago'),
			'confirmacion'=>array('Confirmacion' => 'tns:Confirmacion')
		];
		$outputs = [
			array('return' => 'xsd:Array')
		];

	    $this->server->register('registrarcliente', $inputs['cliente'], $outputs[0], $this->ns, false, 'rpc', 'encoded','Registra un nuevo cliente');
	    $this->server->register('recargarbilletera', $inputs['recarga'], $outputs[0], $this->ns, false, 'rpc', 'encoded','Recarga la billetera');
	    $this->server->register('consultarsaldo', $inputs['saldo'], $outputs[0], $this->ns, false, 'rpc', 'encoded','Muestra el balance disponible');
	    $this->server->register('realizarpago', $inputs['pago'], $outputs[0], $this->ns, false, 'rpc', 'encoded','Inicia una transaccion de pago');
	    $this->server->register('confirmarpago', $inputs['confirmacion'], $outputs[0], $this->ns, false, 'rpc', 'encoded','Valida y procesa la transaccion de pago');

	    if (isset($HTTP_RAW_POST_DATA)) {
	    	$input = $HTTP_RAW_POST_DATA;
	    }
	    else {
	    	$input = implode("\r\n", file('php://input'));
	    }
	    $this->server->service($input);

	}

	// REGISTRAR CLIENTE
	function registrarCliente($data) {
		$this->load->model('Cliente');
		$this->load->library('form_validation');
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('nombres','nombres','required|trim');
		$this->form_validation->set_rules('documento','documento','required|trim|is_unique[clientes.documento]');
		$this->form_validation->set_rules('email','email','required|trim|valid_email|is_unique[clientes.email]');
		$this->form_validation->set_rules('celular','celular','required|trim|is_unique[clientes.celular]');
		if ($this->form_validation->run()===TRUE) {
			$cl = new Cliente();
			unset($cl->id, $cl->updated_at);
			$cl->nombres = $data['nombres'];
			$cl->documento = $data['documento'];
			$cl->email = $data['email'];
			$cl->celular = $data['celular'];
			$cl->created_at = date('Y-m-d H:i:s');
			$q=$this->db->insert('clientes', $cl);
			$error=$this->db->error();
			if ($error['message']) {
				return ['action'=>'REGISTRAR_CLIENTE','status'=>'ERROR',$error['message'],'data'=>null];
			}
			$id=$this->db->insert_id();
			// Create Wallet
			$this->db->reset_query();
			$q1=$this->db->insert('wallet', ['cliente'=>$id,'token'=>sha1($cl->email.date('Y-m-d H:i:s')),'balance'=>0,'created_at'=>date('Y-m-d H:i:s')]);
		}
		else {
			return ['action'=>'REGISTRAR_CLIENTE','status'=>'ERROR','message'=>'Los valores Documento, Email y Celular deben ser unicos','data'=>$this->form_validation->error_array()];
		}
		return ['action'=>'REGISTRAR_CLIENTE','status'=>'OK','message'=>'Se ha registrado el cliente','data'=>null];
	}

	// RECARGAR BILLETERA
	function recargarBilletera($data) {
		$this->load->model('Cliente');
		$this->load->model('Transaccion');
		$this->load->library('form_validation');
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('documento','documento','required|trim');
		$this->form_validation->set_rules('celular','celular','required|trim');
		$this->form_validation->set_rules('valor','valor','required|trim|numeric|regex_match[/^\d+(\.\d{1,2})?$/]');
		if ($this->form_validation->run()===TRUE) {
			$this->db->where('documento',$data['documento']);
			$this->db->where('celular',$data['celular']);
			$rsc = $this->db->get('clientes');
			$error=$this->db->error();
			if ($error['message']) {
				return ['ERROR'=>$error['message']];
			}
			if ($rsc->num_rows()!=1) {
				return ['action'=>'RECARGAR_BILLETERA','status'=>'ERROR','message'=>'Ningun registro coincide con la informacion proporcionada.','data'=>null];
			}
			$cl=$rsc->row(0, 'Cliente');
			$cl->documento = $data['documento'];
			$cl->celular = $data['celular'];
			$oper=new Transaccion();
			unset($oper->id, $oper->updated_at);
			$oper->cliente = $cl->id;
			$oper->estatus = TRUE;
			$oper->tipo = 'recarga';
			$oper->codigo = strtoupper(substr(sha1($cl->email.$data['valor'].date('Y-m-d H:i:s')), rand(0, 34), 6));
			$oper->monto = $data['valor'];
			$oper->created_at = date('Y-m-d H:i:s');
			$q=$this->db->insert('transacciones', $oper);
			$error=$this->db->error();
			if ($error['message']) {
				return ['action'=>'RECARGAR_BILLETERA','status'=>'ERROR','message'=>$error['message'],'data'=>null];
			}
			// Update Wallet
			$walletUpdated = $this->updateWallet($cl->id, 'CREDITO', $oper->monto);
		}
		else {
			return ['action'=>'RECARGAR_BILLETERA','status'=>'ERROR','message'=>'Los valores Documento, Email y Celular deben ser unicos','data'=>$this->form_validation->error_array()];
		}
		return ['action'=>'RECARGAR_BILLETERA', 'status'=>'OK', 'message'=>'Se ha recargado la billetera.', 'data'=>null];
	}

	// CONSULTAR SALDO
	function consultarSaldo($data) {
		$this->load->library('form_validation');
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('documento','documento','required|trim');
		$this->form_validation->set_rules('celular','celular','required|trim');
		if ($this->form_validation->run()===TRUE) {
			$this->db->select('balance');
			$this->db->join('wallet','clientes.id=cliente','inner');
			$this->db->where('documento', $data['documento']);
			$this->db->where('celular', $data['celular']);
			$q=$this->db->get('clientes');
			$error=$this->db->error();
			if ($error['message']) {
				return ['action'=>'CONSULTAR_SALDO', 'status'=>'ERROR', 'message'=>$error['message'], 'data'=>null];
			}
			if ($q->num_rows()<1) {
				return ['action'=>'CONSULTAR_SALDO', 'status'=>'WARNING', 'message'=>'Ningun registro coincide con la informacion proporcionada.', 'data'=>null];
			}
		}
		else {
			return ['action'=>'CONSULTAR_SALDO','status'=>'ERROR','message'=>'Los valores Documento y Celular son obligatorios','data'=>$this->form_validation->error_array()];
		}
		return ['action'=>'CONSULTAR_SALDO', 'status'=>'OK', 'message'=>'Balance disponible.', 'data'=>$q->row(0)->balance];
	}

	// REALIZAR PAGO
	function realizarPago($data) {
		$this->load->library('form_validation');
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('documento','documento','required|trim');
		$this->form_validation->set_rules('compra','compra','required|trim');
		$this->form_validation->set_rules('monto','monto','required|trim|numeric|regex_match[/^\d+(\.\d{1,2})?$/]');
		if ($this->form_validation->run()===TRUE) {
			$this->load->model('Cliente');
			$this->load->model('Transaccion');

			$this->db->where('documento',$data['documento']);
			$q=$this->db->get('clientes');
			$error=$this->db->error();
			if ($error['message']) {
				return ['action'=>'REALIZAR_PAGO', 'status'=>'ERROR', 'message'=>$error['message'], 'data'=>null];
			}
			if ($q->num_rows()<1) {
				return ['action'=>'REALIZAR_PAGO', 'status'=>'WARNING', 'message'=>'Ningun registro coincide con la informacion proporcionada.', 'data'=>null];
			}
			$cl = $q->row(0, 'Cliente');
			$this->db->reset_query();
			$this->db->select('balance');
			$this->db->where('cliente', $cl->id);
			$q1=$this->db->get('wallet');
			if ($q1->row(0)->balance < $data['monto']) {
				return ['action'=>'REALIZAR_PAGO', 'status'=>'ERROR', 'message'=>'El balance es insuficiente para procesar el pago.', 'data'=>null];
			}
			$oper=new Transaccion();
			unset($oper->id, $oper->updated_at);
			$oper->cliente = $cl->id;
			$oper->estatus = FALSE;
			$oper->tipo = 'pago';
			$oper->codigo = strtoupper(substr(sha1($cl->email.$data['monto'].date('Y-m-d H:i:s')), rand(0, 34), 6));
			$oper->monto = $data['monto'];
			$oper->created_at = date('Y-m-d H:i:s');
			$this->db->reset_query();
			$q2=$this->db->insert('transacciones', $oper);
			$error=$this->db->error();
			if ($error['message']) {
				return ['action'=>'REALIZAR_PAGO', 'status'=>'ERROR', 'message'=>$error['message'], 'data'=>null];
			}
			if ($q2) {
				$oper->id=$this->db->insert_id();
				// Enviar codigo por Email
				/*
				$message ='<p>Su codigo de confirmacion es de pago: '..'</p>';
				$message.='<h2>'.$oper->codigo.'</h2>';
				@mail($cl->email, 'Codigo de Confirmacion', $message,"Content.Type: text/html; charset=utf-8\nFrom: PayApp <pagos@$_SERVER[SERVER_NAME]>\nReply-To: no-reply@$_SERVER[SERVER_NAME]");
				*/
			}
		}
		else {
			return ['action'=>'REALIZAR_PAGO','status'=>'ERROR','message'=>'Los valores de Documento, ID de compra y el monto son obligatorios.','data'=>$this->form_validation->error_array()];
		}
		//
		$this->load->library('JWT');
		$jwt = new JWT();
		$time=time();
		$token=[
			'iat'=>$time,
			'exp'=>$time+(5*60*60),
			'data'=>[
				'id'=>$cl->id,
				'doc'=>$cl->documento,
				'oper'=>$oper->id
			]
		];
		return [
			'action'=>'REALIZAR_PAGO', 
			'status'=>'OK',
			'message'=>'Se ha enviado un codigo de confirmacion a su correo electronico.',
			'data'=>['sesion'=>$jwt->encode($token, JWT_SECRET_KEY)]
		];
	}
	
	// CONFIRMAR PAGO
	function confirmarPago($data) {
		$this->load->library('form_validation');
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('sesion','sesion','required|trim');
		$this->form_validation->set_rules('token','token','required|trim|exact_length[6]');
		if ($this->form_validation->run()===TRUE) {
			$this->load->library('JWT');
			$jwt = new JWT();
			$time=time();
			try {
				$d = $jwt->decode($data['sesion'], JWT_SECRET_KEY);
				if ($time>$d->exp) {
					return ['action'=>'CONFIRMAR_PAGO', 'status'=>'WARNING', 'message'=>'Su sesion esta caducada', 'data'=>$d];
				}
				$this->load->model('Cliente');
				$this->load->model('Transaccion');

				$this->db->where('id', $d->data->id);
				$q=$this->db->get('clientes');
				$cl = $q->row(0, 'Cliente');

				$this->db->reset_query();
				$this->db->where('id', $d->data->oper);
				$this->db->where('codigo', $data['token']);
				$q1=$this->db->get('transacciones');
				if ($q1->num_rows()<1) {
					return ['action'=>'CONFIRMAR_PAGO', 'status'=>'ERROR', 'message'=>'No es posible confirmar el pago, los datos son invalidos.', 'data'=>null];
				}
				$trans = $q1->row(0, 'Transaccion');
				if ($trans->estatus) {
					return ['action'=>'CONFIRMAR_PAGO', 'status'=>'OK', 'message'=>'El pago ya ha sido confirmado previamente.', 'data'=>null];
				}
				
				if (!$this->checkWallet($cl->id, $trans->monto)) {
					return ['action'=>'CONFIRMAR_PAGO', 'status'=>'ERROR', 'message'=>'El balance es insuficiente para confirmar el pago.', 'data'=>null];
				}

				$walletUpdated = $this->updateWallet($cl->id, 'DEBITO', $trans->monto);
				if ($walletUpdated) {
					$this->db->reset_query();
					$this->db->where('id', $trans->id);
					$q2=$this->db->update('transacciones', ['estatus'=>TRUE, 'updated_at'=>date('Y-m-d H:i:s')]);
					return ['action'=>'CONFIRMAR_PAGO', 'status'=>'OK', 'message'=>'Su pago ha sido confirmado.', 'data'=>null];
				}
				else {
					return ['action'=>'CONFIRMAR_PAGO', 'status'=>'ERROR', 'message'=>'Su pago no ha podido ser confirmado.', 'data'=>null];
				}
			}
			catch (Exception $jx) {
				return ['action'=>'CONFIRMAR_PAGO', 'status'=>'ERROR', 'message'=>'No es una sesion valida.', 'data'=>null];
			}
		}
		else {
			return ['action'=>'CONFIRMAR_PAGO','status'=>'ERROR','message'=>'Se requiere un token y un id de sesion.','data'=>$this->form_validation->error_array()];
		}
	}

	private function updateWallet($cliente, $tipo, $monto) {
		if ($tipo=='DEBITO' && !$this->checkWallet($cliente, $monto)) {
			return false;
		}
		$q1=$this->db->query('UPDATE wallet SET balance = (balance '. ($tipo=='DEBITO' ? '-' : '+') .$monto.'), updated_at = CURRENT_TIMESTAMP() WHERE cliente = '.$cliente.';');
		$error=$this->db->error();
		if ($error['message']) {
			return false;
		}
		return true;
	}

	private function checkWallet($cliente, $monto){
		$this->db->select('balance');
		$this->db->where('cliente', $cliente);
		$q1=$this->db->get('wallet');
		if ($q1->row(0)->balance < $monto) {
			return false;
		}
		return true;
	}

}