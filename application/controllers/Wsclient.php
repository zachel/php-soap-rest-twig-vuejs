<?php
class Wsclient extends CI_controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Nusoap');
    }

    function index() {

        $this->soapclient = new nusoap_client("http://$_SERVER[SERVER_NAME]/api/server?wsdl", true);
        $err = $this->soapclient->getError();
        if ($err) {
            $this->returnError(['code'=>'-1', 'message'=>$err]);
            exit();
        }
        
        $xml = str_ireplace(['s:','SOAP-ENV:','SOAP:'],'',file_get_contents('php://input'));
        if (empty($xml)) {
            header('Location: /api/server?wsdl');
            return false;
        }
        $objXML = json_decode(json_encode((new SimpleXMLElement($xml))));
        if (!is_object($objXML)){
            $this->returnError(['code'=>'-1', 'message'=>'La estructura del requerimiento es invalida.']);
            exit();
        }

        $action = $this->uri->segment(2);

        switch ($action) {
            case 'registrarcliente':
                if (
                    (!isset($objXML->Body->cliente) || !$objXML->Body->cliente) || 
                    (!isset($objXML->Body->cliente->nombres) || !$objXML->Body->cliente->nombres) ||
                    (!isset($objXML->Body->cliente->documento) || !$objXML->Body->cliente->documento) || 
                    (!isset($objXML->Body->cliente->email) || !$objXML->Body->cliente->email) ||
                    (!isset($objXML->Body->cliente->celular) || !$objXML->Body->cliente->celular)
                ) {
                    $this->returnError(['code'=>'-1', 'actor'=>'RegistrarCliente', 'message'=>'Datos requeridos incompletos','detail'=>'No se han proporcionado los datos requeridos para procesar la peticion.']);
                    exit();
                }
                $params=$objXML->Body->cliente;
                $options=['Cliente'=>['nombres'=>$params->nombres,'documento'=>$params->documento,'email'=>$params->email,'celular'=>$params->celular]];
                break;
            case 'recargarbilletera':
                if (
                    (!isset($objXML->Body->recarga) || !$objXML->Body->recarga) || 
                    (!isset($objXML->Body->recarga->documento) || !$objXML->Body->recarga->documento) || 
                    (!isset($objXML->Body->recarga->celular) || !$objXML->Body->recarga->celular) ||
                    (!isset($objXML->Body->recarga->valor) || !$objXML->Body->recarga->valor)
                ) {
                    $this->returnError(['code'=>'-1', 'actor'=>'RecargarBilletera', 'message'=>'Datos requeridos incompletos','detail'=>'No se han proporcionado los datos requeridos para procesar la peticion.']);
                    exit();
                }
                $params=$objXML->Body->recarga;
                $options=['Recarga'=>['documento'=>$params->documento,'celular'=>$params->celular,'valor'=>$params->valor]];
                break;
            case 'consultarsaldo':
                if (
                    (!isset($objXML->Body->consulta) || !$objXML->Body->consulta) || 
                    (!isset($objXML->Body->consulta->documento) || !$objXML->Body->consulta->documento) || 
                    (!isset($objXML->Body->consulta->celular) || !$objXML->Body->consulta->celular)
                ) {
                    $this->returnError(['code'=>'-1', 'actor'=>'ConsultarSaldo', 'message'=>'Datos requeridos incompletos','detail'=>'No se han proporcionado los datos requeridos para procesar la peticion.']);
                    exit();
                }
                $params=$objXML->Body->consulta;
                $options=['Saldo'=>['documento'=>$params->documento,'celular'=>$params->celular]];
                break;
            case 'realizarpago':
                if (
                    (!isset($objXML->Body->pago) || !$objXML->Body->pago) || 
                    (!isset($objXML->Body->pago->documento) || !$objXML->Body->pago->documento) || 
                    (!isset($objXML->Body->pago->compra) || !$objXML->Body->pago->compra) ||
                    (!isset($objXML->Body->pago->monto) || !$objXML->Body->pago->monto)
                ) {
                    $this->returnError(['code'=>'-1', 'actor'=>'RealizarPago', 'message'=>'Datos requeridos incompletos','detail'=>'No se han proporcionado los datos requeridos para procesar la peticion.']);
                    exit();
                }
                $params=$objXML->Body->pago;
                $options=['Pago'=>['documento'=>$params->documento,'compra'=>$params->compra,'monto'=>$params->monto]];
                break;
            case 'confirmarpago':
                if (
                    (!isset($objXML->Body->pago) || !$objXML->Body->pago) || 
                    (!isset($objXML->Body->pago->sesion) || !$objXML->Body->pago->sesion) || 
                    (!isset($objXML->Body->pago->token) || !$objXML->Body->pago->token)
                ) {
                    $this->returnError(['code'=>'-1', 'actor'=>'ConfirmarPago', 'message'=>'Datos requeridos incompletos','detail'=>'No se han proporcionado los datos requeridos para procesar la peticion.']);
                    exit();
                }
                $params=$objXML->Body->pago;
                $options=['Confirmacion'=>['sesion'=>$params->sesion,'token'=>$params->token]];
                break;
            default:
                $this->returnError(['code'=>'-1', 'actor'=>'AccionDesconocida', 'message'=>'Accion Desconocida','detail'=>'No se reconoce la peticion.']);
                exit();
                break;
        }

        $this->soapclient->call($action, $options);
        if ($this->soapclient->fault) {
            $this->returnError(['code'=>'-1', 'message'=>$this->soapclient->faultstring]);
        }
        else {
            $err = $this->soapclient->getError();
            if ($err) {
                $this->returnError(['code'=>'-1', 'message'=>$err]);
            }
            else {
                header('Content-Type: text/xml; charset=utf-8');
                print $this->soapclient->responseData;
            }
        }
    }

    private function returnError($options = ['code'=>null, 'actor'=>null, 'message'=>null, 'detail'=>null]) {
        if (!is_array($options)) {
            return false;
        }
        $default=['code'=>'-1', 'actor'=>'', 'message'=>'', 'detail'=>''];
        extract(array_merge($default, $options));
        header('Content-Type: text/xml; charset=utf-8');
        $SoapError = new soap_fault($code, $actor, $message, $detail);
        print $SoapError->serialize();
    }

}