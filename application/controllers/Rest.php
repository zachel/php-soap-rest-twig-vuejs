<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

class Rest extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('Nusoap');
    }

    function index()
    {
        $this->soapclient = new nusoap_client("http://$_SERVER[SERVER_NAME]/api/server?wsdl", true);
        $err = $this->soapclient->getError();
        if ($err) {
            print_r($this->returnError(['code'=>'-1', 'message'=>$err]), true);
        }
        $data = json_decode(file_get_contents('php://input'), TRUE);
        if (!is_array($data)) {
            echo json_encode(['error'=>'No es un request valido']); die();
        }

        $action = $this->uri->segment(2);
        switch ($action) {
            case 'registrarcliente':
                $options=['Cliente'=>$data];
            break;
            case 'recargarbilletera':
                $options=['Recarga'=>$data];
            break;
            case 'consultarsaldo':
                $options=['Saldo'=>$data];
            break;
            case 'realizarpago':
                $options=['Pago'=>$data];
            break;
            case 'confirmarpago':
                $options=['Confirmacion'=>$data];
            break;
            default:
                $response=[
                'success'=>true,
                'message'=>'API REST v1.0',
                'data'=>[]
                ];
                print json_encode($response);
                die();
        }

        $this->soapclient->call($action, $options);
        if ($this->soapclient->fault) {
            $this->returnError(['code'=>'-1', 'message'=>$this->soapclient->faultstring]);
        }
        else {
            $err = $this->soapclient->getError();
            if ($err) {
                $this->returnError(['code'=>'-1', 'message'=>$err]);
            }
            else {
                $xml = new SimpleXMLElement($this->soapclient->responseData);
                $xml->registerXPathNamespace('ns', 'urn:server');
                $fields = $xml->xpath('//return');
                $output='';
                foreach( $fields as $header) {
                    $output .= $header->message;
                }
                print_r(json_encode($fields[0]));
            }
        }
    }

    private function returnError($options = ['code'=>null, 'actor'=>null, 'message'=>null, 'detail'=>null]) {
        if (!is_array($options)) {
            return false;
        }
        $default=['code'=>'-1', 'actor'=>'', 'message'=>'', 'detail'=>''];
        extract(array_merge($default, $options));
        $SoapError = new soap_fault($code, $actor, $message, $detail);
        return $SoapError;
    }
}