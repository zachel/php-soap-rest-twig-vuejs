<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require_once('vendor/autoload.php');
class Welcome extends CI_Controller {
	var $data = ['template'=>null];

	public function index() {
		$this->twig->display('front', $this->data);
	}

	function registrarcliente() {
		$this->data['template']='registro';
		$this->twig->display('front', $this->data);
	}

	function recargarbilletera() {
		$this->data['template']='recarga';
		$this->twig->display('front', $this->data);
	}

	function consultarsaldo() {
		$this->data['template']='consulta';
		$this->twig->display('front', $this->data);
	}

	function realizarpago() {
		$this->data['template']='pago';
		$this->twig->display('front', $this->data);
	}

	function confirmarpago() {
		$this->data['template']='confirma';
		$this->twig->display('front', $this->data);
	}
}
