var $modalMessage = Vue.component('modal-message', {
    props: [
        'message',
        'classes',
        'data'
    ],
  template: '<div :class="classes"><strong>{{ message }}</strong> <small class="d-block">{{ data }}</small></div>'
})

Vue.use('modal-message');

var $vm = new Vue({
    el: '#app',
    data: {
        cliente: {
            nombres: '',
            documento: '',
            email: '',
            celular: ''
        },
        recarga: {
            valor: 0
        },
        pago: {
            compra:'',
            monto: 0
        },
        confirmacion: {
            token: ''
        },
        serverResponse: {
            message: '',
            classes: ''
        }
    },
    methods: {
        registrarCliente: function() {
            if(
                this.cliente.nombres &&
                this.cliente.documento &&
                this.cliente.email &&
                this.cliente.celular
            ) {
                $this=this;
                axios.post('/v1/registrarcliente', this.cliente)
                .then(function(response){
                    if (response.data.status=='OK') {
                        $this.serverResponse.classes = 'alert-success';
                        $this.cliente.nombres = '';
                        $this.cliente.documento = '';
                        $this.cliente.email = '';
                        $this.cliente.celular = '';
                    }
                    else {
                        $this.serverResponse.classes = 'alert-danger';
                        $this.serverResponse.data = Object.values(response.data.data).join(', ');
                    }
                    $this.serverResponse.message = response.data.message;
                })
                .catch(function(error){ console.log(error); });
            }
        },
        consultarSaldo: function() {
            if(
                this.cliente.documento &&
                this.cliente.celular
            ) {
                $this=this;
                axios.post('/v1/consultarsaldo', this.cliente)
                .then(function(response){
                    if (response.data.status=='OK') {
                        $this.serverResponse.classes = 'alert-success';
                        $this.serverResponse.message = response.data.message + ' ' + response.data.data;
                    }
                    else {
                        $this.serverResponse.classes = 'alert-danger';
                        $this.serverResponse.message = response.data.message;
                    }
                })
                .catch(function(error){ console.log(error); });
            }
        },
        recargarBilletera: function() {
            if(
                this.cliente.documento &&
                this.cliente.celular &&
                parseFloat(this.recarga.valor)>0
            ) {
                $this=this;
                axios.post('/v1/recargarbilletera', {documento: $this.cliente.documento, celular: $this.cliente.celular, valor: $this.recarga.valor})
                .then(function(response){
                    if (response.data.status=='OK') {
                        $this.serverResponse.classes = 'alert-success';
                        $this.cliente.documento = '';
                        $this.cliente.celular = '';
                        $this.recarga.valor = 0;
                    }
                    else {
                        $this.serverResponse.classes = 'alert-danger';
                    }
                    $this.serverResponse.message = response.data.message;
                })
                .catch(function(error){ console.log(error); });
            }
        },
        realizarPago: function() {
            if(
                this.cliente.documento &&
                this.pago.compra &&
                parseFloat(this.pago.monto)>0
            ) {
                $this=this;
                axios.post('/v1/realizarpago', {documento: $this.cliente.documento, compra: $this.pago.compra, monto: $this.pago.monto})
                .then(function(response){
                    if (response.data.status=='OK') {
                        $this.serverResponse.classes = 'alert-success';
                        $this.cliente.documento = '';
                        $this.pago.compra = '';
                        $this.pago.monto = 0;
                        localStorage.setItem('sesion', response.data.data.sesion);
                    }
                    else {
                        $this.serverResponse.classes = 'alert-danger';
                        $this.serverResponse.data = Object.values(response.data.data).join(', ');
                    }
                    $this.serverResponse.message = response.data.message;
                })
                .catch(function(error){ console.log(error); });
            }
        },
        confirmarPago: function() {
            if (!this.confirmacion.token || this.confirmacion.token.length != 6) {
                this.serverResponse.classes = 'alert-danger';
                this.serverResponse.data = 'Debe proporcionar el codigo de 6 digitos recibido en su email.';
                return;
            }
            if (!localStorage.getItem('sesion')) {
                this.serverResponse.classes = 'alert-danger';
                this.serverResponse.data = 'No existe una sesion activa.';
                return;
            }
            if(
                localStorage.getItem('sesion') &&
                this.confirmacion.token
            ) {
                $this=this;
                axios.post('/v1/confirmarpago', {sesion: localStorage.getItem('sesion'), token: $this.confirmacion.token})
                .then(function(response){
                    if (response.data.status=='OK') {
                        $this.serverResponse.classes = 'alert-success';
                        $this.confirmacion.token = '';
                        localStorage.removeItem('sesion');
                    }
                    else {
                        $this.serverResponse.classes = 'alert-danger';
                        $this.serverResponse.data = Object.values(response.data.data).join(', ');
                    }
                    $this.serverResponse.message = response.data.message;
                })
                .catch(function(error){ console.log(error); });
            }
        }
    }
});